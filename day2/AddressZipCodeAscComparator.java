package day2;

import java.util.Comparator;

public class AddressZipCodeAscComparator implements Comparator<Address>{

    public int compare(Address address1, Address address2){
        return address1.getZipCode() - address2.getZipCode();
    }
    
}
