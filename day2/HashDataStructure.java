package day2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashDataStructure {

    public static void main(String[] args) {
        Address address1 = new Address("Bangalore", "Karnataka", 577142);
        Address address2 = new Address("Bangalore", "Karnataka", 577142);
        Address address3 = new Address("Mysore", "Karnataka", 577142);

        System.out.println("Hashcode of address1 is " + address1.hashCode());
        //System.out.println("Hashcode of address2 is " + address2.hashCode());
       // System.out.println("Hashcode of address3 is " + address3.hashCode());

        Set<Address> addresses = new HashSet<>();
        addresses.add(address1);
        addresses.add(address2);
        addresses.add(address3);

        System.out.println("Size of the addresse set is "+ addresses.size());

        
        System.out.println("Hashcode of address1 after changing the zip code is " + address1.hashCode());
        System.out.println("Is address1 present >> "+ addresses.contains(address1));

        Iterator<Address> iterator = addresses.iterator();

        while(iterator.hasNext()){
            Address address = iterator.next();
            System.out.println("Address :: "+ address);
        }

        
    }
    
}
