package day2;
import java.util.*;
public class ArrayListDemo1 {
    public static void main(String args[])
    {
         List<String> names= new ArrayList<String>();
         names.add("Shraddha");
         names.add("Abhinav");
         names.add("Yashwanth");
         names.forEach(name -> System.out.println(name));
         System.out.println("Name at 0th position is : "+names.get(0));
         System.out.println("IS shraddha present : "+names.contains("Shraddha"));

    }
}
