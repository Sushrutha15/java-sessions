package day2;

import java.util.Iterator;
import java.util.Set; 
import java.util.TreeSet;

public class TreesetDemo {

    public static void main(String[] args) {
        Address address1 = new Address("Bangalore", "Karnataka", 277142);
        Address address2 = new Address("Mangalore", "Karnataka", 377242);
        Address address3 = new Address("Mysore", "Karnataka", 577142);


        AddressCityAscComparator cityAsc = new AddressCityAscComparator();
        AddressCityDescComparator cityDesc = new AddressCityDescComparator();

        AddressZipCodeAscComparator zipCodeAsc = new AddressZipCodeAscComparator();
        AddressZipCodeDescComparator zipCodeDesc = new AddressZipCodeDescComparator();

        Set<Address> addresses = new TreeSet<>();

        addresses.add(address1);
        addresses.add(address2);
        addresses.add(address3);

        System.out.println("Size of treeset is "+ addresses.size());
        Iterator<Address> it = addresses.iterator();
        
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
