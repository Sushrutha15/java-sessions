package day1;

public class InstanceVariables {
    
    private int intValue;
    private boolean booleanValue;
    private String stringValue;
    private double doubleValue;
    private char charValue;

    public static void main(String[] args) {
        InstanceVariables obj = new InstanceVariables();

        int uninitializedVariable = 55;

        System.out.println(" Int value :: "+ obj.intValue);
        System.out.println(" Boolean value :: "+ obj.booleanValue);
        System.out.println(" String value :: "+ obj.stringValue);
        System.out.println(" double value :: "+ obj.doubleValue);
        System.out.println(" char value :: "+ obj.charValue);
        
        System.out.println(" uniitialized value :: "+ uninitializedVariable);
    }

}
