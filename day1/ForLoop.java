package day1;
public class ForLoop {
    private int row,col;
    private int [][] matrix;

    public ForLoop(int matrix){
        this.row = matrix;
        this.col = matrix;
        this.matrix = new int[row][col];
    }
    public static void main(String[] args) {
        int initialValue = 100;
        ForLoop matrixDem= new ForLoop(Integer.parseInt(args[0]));

        int [][] matrix = populateArray(initialValue, Integer.parseInt(args[0]));

        printArray(matrix, Integer.parseInt(args[0]));
   }

   private static int[][] populateArray(int initialValue, int size){
        //array initialization
        int [][] matrix = new int[size][size];
        for(int rowIndex = 0; rowIndex < size; rowIndex++){
            for(int colIndex = 0; colIndex < size; colIndex++){
                matrix[rowIndex][colIndex] = initialValue;
                initialValue++;
            }
        }
        return matrix;
    }

    private static void printArray(int [][] matrix, int size){
            //array print
            for(int rowIndex = 0; rowIndex < size; rowIndex++){
                for(int colIndex = 0; colIndex < size; colIndex++){
                    System.out.print(matrix[rowIndex][colIndex] + " ");
                }
                System.out.println();
            }       
    }
}